# frozen_string_literal: true

class Sale < ApplicationRecord
  require 'csv'
  validates :customer_name, presence: true
  validates :item_description, presence: true
  validates :item_price, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :merchant_name, presence: true
  validates :merchant_address, presence: true

  class << self
    def import(file)
      CSV.foreach(file.path, encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all) do |row|
        Sale.create! row.to_hash
      end
    end

    def total_sales_revenue!
      total = 0
      Sale.all.each do |sale|
        raise unless sale.valid?

        total += sale.item_price * sale.quantity
      end
      total
    end
  end
end
