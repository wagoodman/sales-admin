# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  before_action :authenticate_user!

  private

  def authenticate_user!
    unless logged_in?
      redirect_to login_url, flash: { danger: 'Please login to continue' }
    end
  end
end
