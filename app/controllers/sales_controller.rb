# frozen_string_literal: true

class SalesController < ApplicationController
  def index
    @sales = Sale.all
    begin
      @revenue = Sale.total_sales_revenue!
    rescue StandardError => e
      Rails.logger.error "could not calculate revenue: #{e}"
      @revenue = -1
    end
  end

  def import
    if params[:file]
      begin
        Sale.import(params[:file])
        redirect_to root_url, flash: { success: 'CSV Imported' }
      rescue
        redirect_to root_url, flash: { danger: 'Unable to process CSV file.' }
      end
    else
      redirect_to root_url, flash: { danger: 'No file given. Select a CSV file.' }
    end
  end
end
