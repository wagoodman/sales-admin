# frozen_string_literal: true

User.create(name: 'Agent Smith',
            email: 'asmith@matrix.com',
            password: 'thereisnospoon',
            password_confirmation: 'thereisnospoon')
