# frozen_string_literal: true

Rails.application.routes.draw do
  root 'sales#index'

  resources :sales do
    collection do
      post :import
    end
  end

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
end
