# frozen_string_literal: true

require 'test_helper'

class SingleSaleTest < ActiveSupport::TestCase
  def setup
    @sale = Sale.new(customer_name: 'Jack', item_description: 'Premium',
                     item_price: 149.95, quantity: 1,
                     merchant_name: 'Carpenter', merchant_address: '99')
  end

  test 'should be valid' do
    assert @sale.valid?
  end

  test 'valid prices' do
    prices = %w[0 10 10000 1.39 1.23894623894]
    prices.each do |price|
      @sale.item_price = price
      assert @sale.valid?, "price=#{price.inspect} should be valid"
    end
  end

  test 'invalid prices' do
    prices = %w[-10]
    prices.each do |price|
      @sale.item_price = price
      assert_not @sale.valid?, "price=#{price.inspect} should be valid"
    end
  end

  test 'valid quantities' do
    quantities = %w[0 10 10000]
    quantities.each do |quantity|
      @sale.quantity = quantity
      assert @sale.valid?, "quantity=#{quantity.inspect} should be valid"
    end
  end

  test 'invalid quantities' do
    quantities = %w[-10 1.39 1.23894623894]
    quantities.each do |quantity|
      @sale.quantity = quantity
      assert_not @sale.valid?, "quantity=#{quantity.inspect} should be valid"
    end
  end
end

class RevenueSalesTest < ActiveSupport::TestCase
  test 'valid model values results in correct revenue' do
    Sale.new(customer_name: '1', item_description: '1', item_price: 150.01,
             quantity: 1, merchant_name: '1', merchant_address: '1').save
    Sale.new(customer_name: '2', item_description: '2', item_price: 100,
             quantity: 2, merchant_name: '2', merchant_address: '2').save
    Sale.new(customer_name: '3', item_description: '3', item_price: 1.1,
             quantity: 3, merchant_name: '3', merchant_address: '3').save

    assert_equal Sale.total_sales_revenue!, 353.31
  end

  test 'negative price values results in exception' do
    # note: we cannot save invalid data to a model and have it be available to
    # Sale.all, so we will stub out the call.
    mock = Minitest::Mock.new
    def mock.apply
      [
        Sale.new(customer_name: '1', item_description: '1', item_price: -150.01,
                 quantity: 1, merchant_name: '1', merchant_address: '1'),
        Sale.new(customer_name: '2', item_description: '2', item_price: -100,
                 quantity: 2, merchant_name: '2', merchant_address: '2'),
        Sale.new(customer_name: '3', item_description: '3', item_price: -1.1,
                 quantity: 3, merchant_name: '3', merchant_address: '3')
      ]
    end

    Sale.stub :all, mock do
      assert_raise(Exception) do
        Sale.total_sales_revenue!
      end
    end
  end

  test 'negative quantity values results in exception' do
    # note: we cannot save invalid data to a model and have it be available to
    # Sale.all, so we will stub out the call.
    mock = Minitest::Mock.new
    def mock.apply
      [
        Sale.new(customer_name: '1', item_description: '1', item_price: 150.01,
                 quantity: -1, merchant_name: '1', merchant_address: '1'),
        Sale.new(customer_name: '2', item_description: '2', item_price: 100,
                 quantity: -2, merchant_name: '2', merchant_address: '2'),
        Sale.new(customer_name: '3', item_description: '3', item_price: 1.1,
                 quantity: -3, merchant_name: '3', merchant_address: '3')
      ]
    end

    Sale.stub :all, mock do
      assert_raise(Exception) do
        Sale.total_sales_revenue!
      end
    end
  end

  test 'fractional quantity values results in exception' do
    # note: we cannot save invalid data to a model and have it be available to
    # Sale.all, so we will stub out the call.
    mock = Minitest::Mock.new
    def mock.apply
      [
        Sale.new(customer_name: '1', item_description: '1', item_price: 150.01,
                 quantity: 0.5, merchant_name: '1', merchant_address: '1'),
        Sale.new(customer_name: '2', item_description: '2', item_price: 100,
                 quantity: 0.2, merchant_name: '2', merchant_address: '2'),
        Sale.new(customer_name: '3', item_description: '3', item_price: 1.1,
                 quantity: 0.3, merchant_name: '3', merchant_address: '3')
      ]
    end

    Sale.stub :all, mock do
      assert_raise(Exception) do
        Sale.total_sales_revenue!
      end
    end
  end
end
