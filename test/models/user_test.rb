# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: 'Agent smith',
                     email: 'asmith@matrix.org',
                     password: 'supersecure',
                     password_confirmation: 'supersecure')
  end

  test 'should be valid' do
    assert @user.valid?
  end

  test 'name should not be empty' do
    @user.name = ' '
    assert_not @user.valid?
  end

  test 'valid email addresses' do
    valid_addresses = %w[something@blah.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test 'invalid email addresses' do
    invalid_addresses = %w[something@blah,com foo_at_bar.org foo@bar_baz.com
                           foo@bar+baz.com agent.smith@example.]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test 'unique email addresses' do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test 'password should be present (nonblank)' do
    @user.password = @user.password_confirmation = ' ' * 6
    assert_not @user.valid?
  end

  test 'password should have a minimum length' do
    @user.password = @user.password_confirmation = 'a' * 5
    assert_not @user.valid?
  end
end
