# Sales Admin

solution to problem posted here: https://github.com/panoplymedia/challenges/tree/master/sales_admin

## Getting Started

Note: this requires `docker` and `docker-compose` to be installed.

Grab the docker images, install dependencies, setup the postgres database, run migrations, and seed data with:
```
./setup.sh
```

Run the application and database with:
```
docker-compose up
```

Head to `localhost:3000`. Default login is `asmith@matrix.com` (password: `thereisnospoon`)

Note: Upon processing a CSV and an error is shown, check the output of `docker-compose up` for more details.

## Assumptions

- A CSV with a header of `Customer Name,Item Description,Item Price,Quantity,Merchant Name,Merchant Address` is provided.
- No field is allowed to be empty (it would be bad to track revenue without knowing where it came from).
- Uploading multiple CSVs will result in appending to the existing table (previous data will not be purged).
- There is not a need to de-duplicate given data (it is possible and valid for a customer to purchase the same thing twice).

## Background

Your company just acquired Acme Cult Hero Supplies. They have been using a CSV worksheet to track sales data, and you need to transform that into a web application to track revenue.

## Functional requirements

Using the web framework of your choice, deliver an application that meets the following requirements:

* Provides an interface for a user to upload the salesdata.csv file in this directory
* Parses and persists the information in the salesdata.csv file
* Calculates and displays the total sales revenue to the user

Bonus points if you add authentication.

_Ideally you shouldn't spend more than 4-5 hours on your solution, but take as much time as you want._

## Delivery requirements

Please provide instructions for installing and running your application, including all dependencies. The simpler, the better, but we do use PostgreSQL if you want use that as a data store.

Think about things like:

* Testing
* How to store the data
* How would your solution differ if it had to scale?

Please submit your solution as a pull request, or package it up and send it to doug.ramsay@megaphone.fm.

## Credits

Yes, this challenge is copied from the LivingSocial code challenge. I helped put that together, so hopefully nobody will mind that much.